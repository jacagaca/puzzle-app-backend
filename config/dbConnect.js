let connectDB = () => {
  const mongoose = require("mongoose");
  let URL = `${process.env.DB_PROTOCOL}://${process.env.DB_HOST}/${process.env.DB_NAME}`;
  console.log(process.env.DB_PROTOCOL);
  mongoose.connect(URL, {
    autoIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", function () {
    console.log("Connect db OK");
  });

  return mongoose;
};

module.exports = connectDB;
