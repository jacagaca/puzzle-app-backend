let recordService = require("../services/recordService");

function getFromToRecordsController(req, res) {
  const from = req.body.from;
  const to = req.body.to;

  recordService
    .getFromToRecords(from, to)
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.status(500).json(err);
    });
}

function addRecordController(req, res) {
  const username = req.body.username;
  const record = req.body.record;

  recordService
    .addRecord(username, record)
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.status(500).json(err);
    });
}
function addRecordController(req, res) {
  const username = req.body.username;
  const record = req.body.record;

  recordService
    .addRecord(username, record)
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.status(500).json(err);
    });
}

// function deleteRecordController(req, res) {
//   recordService
//     .deleteRecord(req.params.id)
//     .then((data) => {
//       console.log(data);
//     })
//     .catch((err) => {
//       res.status(500).json(err);
//     });
// }

module.exports = {
  addRecordController,
  getFromToRecordsController,
};
