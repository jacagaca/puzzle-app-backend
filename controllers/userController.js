const userService = require("../services/userService");
const bcrypt = require("bcrypt");
const jwtUtils = require("../util/jwtComponent");
const { validationResult } = require("express-validator");
const SALT_ROUND = 10;
const jwt = require("jsonwebtoken");

function getAllUserController(req, res) {
  userService
    .getAllUser()
    .then(function (data) {
      return res.json({
        error: false,
        status: 200,
        message: "Get all user OK",
        data: data,
      });
    })
    .catch(function () {
      return res.json({
        error: true,
        status: 500,
        message: "Get all user fail",
      });
    });
}

function getDetailUserController(req, res) {
  userService
    .getDetailUser(req.params.id)
    .then(function (data) {
      if (!data) {
        return res.json({
          error: false,
          status: 200,
          message: "User not exist",
        });
      } else {
        return res.json({
          error: false,
          status: 200,
          message: "Get detail user OK",
          data: data,
        });
      }
    })
    .catch(function () {
      return res.json({
        error: true,
        status: 500,
        message: "Get detail user fail",
      });
    });
}

function registerController(req, res) {
  let { username, password } = req.body;

  bcrypt.genSalt(SALT_ROUND, function (err, salt) {
    bcrypt.hash(password, salt, function (err, hash) {
      userService
        .register(username, hash)
        .then((user) => {
          const createUser = user;
          jwtUtils
            .generateJWT({ _id: user._id }, process.env.JWT_SECRET, "1d")
            .then((token) => {
              return res.json({
                error: false,
                status: 200,
                message: "Register OK",
                token: token,
                data: createUser,
              });
            })
            .catch(function (err) {
              return res.json({
                error: true,
                status: 500,
                message: err.message,
              });
            });
        })
        .catch(function (err) {
          return res.json({
            error: true,
            status: 500,
            message: err.message,
          });
        });
    });
  });
}

function loginController(req, res) {
  let { username, password } = req.body;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      error: true,
      status: 500,
      message: "Username and Password must have atleast 6 characters",
    });
  }

  userService
    .checkUsername(username)
    .then((user) => {
      if (!user) {
        return res.json({
          error: true,
          status: 500,
          message: "Username or Password is incorrect",
        });
      }

      bcrypt.compare(password, user.password, function (err, result) {
        if (result) {
          jwtUtils
            .generateJWT({ _id: user._id }, process.env.JWT_SECRET, "1d")
            .then((token) => {
              return res.json({
                data: user,
                error: false,
                status: 200,
                message: "Login OK",
                token: token,
              });
            });
        } else {
          return res.json({
            error: true,
            status: 500,
            message: "Login fail",
          });
        }
      });
    })
    .catch(function () {
      return res.json({
        error: true,
        status: 500,
        message: "Internal server error",
      });
    });
}

function checkUser(req, res) {
  const token = req.body.token;

  if (token) {
    try {
      const user = jwt.verify(token, process.env.JWT_SECRET);

      userService.getDetailUser(user._id).then((user) => {
        if (!user) {
          return res.json({
            error: true,
            status: 500,
            message: "Username doesn't exists",
          });
        } else {
          return res.json({
            error: false,
            status: 500,
            data: user,
          });
        }
      });
    } catch (error) {
      res.json({
        error: false,
        status: 500,
        message: "Internal server error",
      });
    }
  } else {
    res.json({
      error: false,
      status: 500,
      message: "Token not found",
    });
  }
}

module.exports = {
  registerController,
  getAllUserController,
  getDetailUserController,
  loginController,
  checkUser,
};
