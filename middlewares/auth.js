const userService = require("../services/userService");
const jwt = require("jsonwebtoken");

//if user exists
async function checkUser(req, res, next) {
  const user = await userService.checkUsername(req.body.username);

  if (!user) {
    next();
  } else {
    return res.json({
      error: false,
      status: 400,
      message: "Account exists",
    });
  }
}

async function checkAuth(req, res, next) {
  const token = req.cookies.token;

  if (token) {
    const decodeUser = jwt.verify(token, process.env.JWT_SECRET);
    const user = await userService.getDetailUser(decodeUser._id);
    if (user) {
      req.user = user;
      next();
    } else {
      return res.json({
        error: false,
        status: 400,
        message: "Please login",
      });
    }
  }
  next();
}

module.exports = {
  checkUser,
  checkAuth,
};
