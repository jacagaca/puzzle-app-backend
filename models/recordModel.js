let mongoose = require("mongoose");

const Schema = mongoose.Schema;

const recordSchema = new Schema(
  {
    username: { type: String, required: true },
    record: { type: Number, required: true },
    idRecord: {
      type: String,
      ref: "user",
    },
  },
  {
    collection: "record",
    timestamps: true,
  }
);

const RecordModel = mongoose.model("record", recordSchema);

module.exports = RecordModel;
