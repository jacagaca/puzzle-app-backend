const express = require("express");
const recordController = require("../controllers/recordController");
const router = express.Router();
const { body } = require("express-validator");

router.post("/date", recordController.getFromToRecordsController);

router.post(
  "/",
  [body("username").isLength({ min: 6, max: 12 })],

  recordController.addRecordController
);

module.exports = router;
