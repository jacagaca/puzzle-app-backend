const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();
const authMiddlewares = require("../middlewares/auth");
const { body } = require("express-validator");
//check
router.post(
  "/login",
  [
    body("username").isLength({ min: 6, max: 12 }),
    body("password").isLength({ min: 6, max: 12 }),
  ],

  userController.loginController
);

router.post("/", userController.checkUser);

router.post(
  "/register",
  [
    body("username").isLength({ min: 6, max: 12 }),
    body("password").isLength({ min: 6, max: 12 }),
  ],

  userController.registerController
);

router.get("/", authMiddlewares.checkUser);
router.get("/check-user", userController.checkUser);

module.exports = router;
