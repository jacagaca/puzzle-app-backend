require("dotenv").config();
const express = require("express");
const cors = require("cors");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const recordRouter = require("./routers/records");
const userRouter = require("./routers/user");
const connectDB = require("./config/dbConnect");
const app = express();

const authMiddlewares = require("./middlewares/auth");

const router = express.Router();
router.get("/", authMiddlewares.checkAuth);

app.use(cors());

connectDB();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use("/public", express.static(path.join(__dirname, "/public")));

app.use("/api/records", recordRouter);
app.use("/api/users", userRouter);

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/", (req, res, next) => {
  res.sendFile(path.join(__dirname, "/index.html"));
});

app.listen(process.env.PORT || 4000);
