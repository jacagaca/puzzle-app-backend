let RecordModel = require("../models/recordModel");

function getAllRecords() {
  return RecordModel.find({});
}

function getFromToRecords(dateFrom, dateTo) {
  return RecordModel.find({
    createdAt: {
      $gte: dateFrom,
      $lt: dateTo,
    },
  });
}

function addRecord(username, record) {
  return RecordModel.create({
    username,
    record,
  });
}

function deleteRecord(id) {
  return RecordModel.deleteOne({
    _id: id,
  });
}

module.exports = {
  getAllRecords,
  addRecord,
  getFromToRecords,
  deleteRecord,
};
