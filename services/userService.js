let UserModel = require("../models/userModel");
function register(username, password) {
  return UserModel.create({
    username,
    password,
  });
}

function login(username, password) {
  return UserModel.findOne({ username, password });
}

function getAllUser() {
  return UserModel.find({});
}

function checkUsername(username) {
  return UserModel.findOne({ username });
}

function getDetailUser(id) {
  return UserModel.findOne({ _id: id });
}

module.exports = {
  register,
  getAllUser,
  getDetailUser,
  login,
  checkUsername,
};
