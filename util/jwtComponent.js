const jwt = require("jsonwebtoken");

const generateJWT = (values, secret, expiresIn) => {
  return new Promise((resolve, reject) => {
    try {
      let token = jwt.sign(values, secret, {
        algorithm: "HS512",
        expiresIn: expiresIn,
      });
      resolve(token);
    } catch (error) {
      reject(error.message);
    }
  });
};

module.exports = {
  generateJWT,
};
